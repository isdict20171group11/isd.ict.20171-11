import java.util.Scanner;

/**
 * Created by HarDToBelieve on 11/21/2017.
 */
public class Main {
    public static void main (String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Input A1: ");
        double A1 = reader.nextInt();
        System.out.print("Input B1: ");
        double B1 = reader.nextInt();
        System.out.print("Input C1: ");
        double C1 = reader.nextInt();
        System.out.print("Input A2: ");
        double A2 = reader.nextInt();
        System.out.print("Input B2: ");
        double B2 = reader.nextInt();
        System.out.print("Input C2: ");
        double C2 = reader.nextInt();

        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        linear.calculate(A1,B1,C1,A2,B2,C2);
    }
}

public class Solve_Linear_Equation {

    public Solve_Linear_Equation(){

    }

    public String calculate(double A1, double B1, double C1, double A2, double B2, double C2){

        if(A1==0&&A2==0&&B1==0&&B2==0&&C1==0&&C2==0){
            return "Infinity Solutions";
        }

        double D =(A1*B2-A2*B1);
        if(D==0){
            if(A1==0 && B1 ==0 && C1 !=0) {
                return "No Solution";

            }
            if(A2==0 && B2 ==0){
                if(C2!=0){
                    return "No Solution";
                }
                else {
                    return "Infinity Solutions";
                }
            }
            if(A1==0 && B1 ==0 && C1 ==0) {
                return "Infinity Solutions";
            }
            if(A1==0 && A2 ==0){
                if(C1/B1 == C2/B2){
                    return "Infinity Solutions";
                }
                else {
                    return "No Solution";
                }
            }
            if(B1==0 && B2 ==0){
                if(C1/A1 == C2/A2){
                    return "Infinity Solutions";
                }
                else {
                    return "No Solution";
                }
            }
            if(C2==C1*A2/A1){
                return "Infinity Solutions";
            }else {
                return "No Solution";
            }
        } else{
            double y =((C1*A2/A1-C2)/(B1*A2/A1-B2));
            double x = (C1-B1*y)/A1;
            return "One Solution: x=" + String.valueOf(x) + " and y=" + String.valueOf(y);

        }


    }
}

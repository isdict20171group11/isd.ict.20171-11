import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by HarDToBelieve on 11/21/2017.
 */
public class BlackBox {
    @Test
    public void testNosolution() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(1,2,4,2,4,5));
    }

    @Test
    public void testInfinitysolution() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(1,3,5,3,9,15));
    }

    @Test
    public void testOnesolution1() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("One Solution: x=0.5 and y=4.0", linear.calculate(2,1,5,4,1,6));
    }

    @Test
    public void testOnesolution2() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("One Solution: x=2.3333333333333335 and y=0.0", linear.calculate(3,5,7,9,9,21));
    }
}
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by HarDToBelieve on 11/21/2017.
 */
public class WhiteBox {
    @Test
    public void testC01() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("One Solution: x=-0.3333333333333335 and y=1.6666666666666667", linear.calculate(1, 2, 3, 4, 5, 7));
    }

    @Test
    public void testC02() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(1, 2, 3, 1, 2, 4));
    }

    @Test
    public void testC03() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(1, 2, 3, 0, 0, 1));
    }

    @Test
    public void testC04() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(0, 1, 2, 0, 2, 4));
    }

    @Test
    public void testC05() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(1, 0, 3, 2, 0, 6));
    }

    @Test
    public void testC06() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(1, 2, 3, 2, 4, 5));
    }

    @Test
    public void testC100() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(0, 0, 0, 0, 0, 0));
    }

    @Test
    public void testC101() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("One Solution: x=1.8571428571428572 and y=0.2857142857142857", linear.calculate(2, 1, 4, 3, 5, 7));
    }

    @Test
    public void testC102() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(0, 0, 4, 13, 5, 4));
    }

    @Test
    public void testC103() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(3, 1, 7, 0, 0, 4));
    }

    @Test
    public void testC104() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(6, 9, 6, 0, 0, 0));
    }

    @Test
    public void testC105() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(0, 0, 0, 9, 2, 1));
    }

    @Test
    public void testC106() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(0, 3, 4, 0, 9, 12));
    }

    @Test
    public void testC107() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(0, 7, 1, 0, 7, 2));
    }

    @Test
    public void testC108() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(2, 0, 6, 5, 0, 15));
    }

    @Test
    public void testC109() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(2, 0, 7, 7, 0, 2));
    }

    @Test
    public void testC110() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("Infinity Solutions", linear.calculate(2, 1, 6, 4, 2, 12));
    }

    @Test
    public void testC111() {
        Solve_Linear_Equation linear = new Solve_Linear_Equation();
        assertEquals("No Solution", linear.calculate(5, 6, 1, 10, 12, 9));
    }
}
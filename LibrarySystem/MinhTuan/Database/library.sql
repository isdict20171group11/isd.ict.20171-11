-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 24, 2017 lúc 05:12 AM
-- Phiên bản máy phục vụ: 10.1.26-MariaDB
-- Phiên bản PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `library`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bookcopyinfo`
--

CREATE TABLE `bookcopyinfo` (
  `copyID` int(10) NOT NULL,
  `bookID` int(10) NOT NULL,
  `author` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bookinfo`
--

CREATE TABLE `bookinfo` (
  `bookID` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `publisher` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  ADD PRIMARY KEY (`copyID`),
  ADD KEY `book_constraint` (`bookID`);

--
-- Chỉ mục cho bảng `bookinfo`
--
ALTER TABLE `bookinfo`
  ADD PRIMARY KEY (`bookID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  MODIFY `copyID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `bookinfo`
--
ALTER TABLE `bookinfo`
  MODIFY `bookID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  ADD CONSTRAINT `book_constraint` FOREIGN KEY (`bookID`) REFERENCES `bookinfo` (`bookID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

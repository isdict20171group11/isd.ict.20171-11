package com.itss;
import com.itss.basic.*;
import java.util.ArrayList;

/**
 * Created by HarDToBelieve on 10/17/2017.
 */

public class BookCopyRegistrationForm extends BasicForm {
	/**
	 * Initialize every component in BookCopyRegistrationForm
	 */
	public BookCopyRegistrationForm() {
		// TODO: 
		// - init button, text, ...
		// - add listeners to buttons
	}

	/**
	 * Get the status of a book by book id
	 * @param bookID the id of book
	 */
	public static void getBookStatus(String bookID) {
		// TODO: 
		// - Call method getBookStatus of BookCopyRegistrationController
	}

	/**
	 * Add copies of a book
	 * @param copies: copy instance which you want to add
	 */
	public static void addCopies(ArrayList<BookCopyInfo> copies) {
		// TODO:
		// - Create a new book copy controller
		// - Call method its saveData
	}	

}
package com.itss;
import com.itss.basic.*;

/**
 * Created by HarDToBelieve on 10/17/2017.
 */

public class BookInfo extends BasicModel {
	private String title;
	private String author;
	private String publisher;
	private String type;
	private double price;
	private String bookID;

	private final String endpoint = "http://localhost/bookinfo";
	private final String keyLocation = ".keydb";

	/**
	 * Init attribute
	 * @param title title of the book
	 * @param author author of the book
	 * @param publisher publisher of the book
	 * @param type type of the book
	 * @param price price of the book
	 * @param bookID id of the book
	 */
	public BookInfo(String title, String author, String publisher, String type, double price, String bookID) {
		// TODO:
		// - init attribute
		// - connect to db
	}

	/**
	 * Get the status of a book by book id
	 * @param bookID the id of book
	 * @return BookInfo instance
	 * @throws BookNotFoundException if there is no such book
	 */
	public static BookInfo getBookByID(String bookID) throws BookNotFoundException {
		// TODO: 
		// - query to DB
		// - return "Available" if found out, or vice versa
		return new BookInfo("", "", "", "", 0, "");
	}

	/**
	 * Insert book information to db
	 * @param book: book instance which need to be added
	 * @throws InsertDBException if there are any errors
	 */
	public static void insertBook(BookInfo book) throws InsertDBException {
		// TODO:
		// - query to DB
		// - throw exception if it exist
	}
}
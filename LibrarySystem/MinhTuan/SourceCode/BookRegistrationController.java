package com.itss;
import com.itss.basic.*;

/**
 * Created by HarDToBelieve on 10/17/2017.
 */

public class BookRegistrationController extends BasicController {
	private BookInfo book;
	private BookRegistrationForm form;

	/**
	 * Constructor
	 * @param book list of copies
	 * @param form registration form
	 */
	public BookRegistrationController(BookInfo book, 
											BookRegistrationForm form) {
		// TODO:
		// - init attributes
	}

	/**
	 * Validate the book
	 * @param bookID: the id of book
	 * @return status of the book ( whether "Available" or "Duplicated" )
	 */
	public static String validateData(String bookID) {
		// TODO: 
		// - Call method getBookByID of BookInfo
		// - return "Available" if not catch any exception or vice versa
		return new String("Available");
	}

	/**
	 * Add a copy of a book
	 * @throws AddBookException if cannot insert copy to database
	 */
	public void saveData() throws AddBookException {
		// TODO:
		// - Call method validateBook
		// - Generate code for the book
		// - Call method addBook of BookInfo
	}
}
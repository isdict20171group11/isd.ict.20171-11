/**
 * Created by hoang on 17/10/2017.
 */
import java.sql.SQLException;

public class BorrowerInfo {
    private boolean isHUST;
    private boolean isBorrowAble;
    private int currentBorrowingBook;

    /**
     * Get borrower status
     * @return boolean
     */
    public boolean getBorrowerStatus() {
        return isBorrowAble;
    }

    /**
     * Update borrower current book to the database
     */
    public void setNumberCurrentBook(int number) {
        this.currentBorrowingBook = number;
    }

    /**
     * Update borrower status information to the database
     */
    public void setBorrowerStatus(boolean isAble) {
        this.isBorrowAble = isAble;
    }

    /**
     * Get number of current book
     * @return int
     */
    public int getNumberCurrentBook() {
        return this.currentBorrowingBook;
    }

    /**
     * Update information to database
     *
     * @throws SQLException if there was a problem when connect or writing data to database
     */
    public void updateQuery() throws SQLException {
        ;
    }
}
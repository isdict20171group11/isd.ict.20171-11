/**
 * Created by hoang on 18/10/2017.
 */
public class BookDeleteController {


    private BookInfo book;
    private BookDeleteEntry form;

    /**
     * Constructor
     * @param book list of copies
     * @param form registration form
     */
    public BookDeleteController(BookInfo book,
                                      BookDeleteEntry form) {
        // TODO:
        // - init attributes
    }

    /**
     * Validate the book
     * @param bookID: the id of book
     * @return status of the book ( whether "Available" or "Duplicated" )
     */
    public static String validateData(String bookID) {
        // TODO:
        // - Call method getBookByID of BookInfo
        // - return "Available" if not catch any exception or vice versa
        return new String("Available");
    }

    /**
     * Delete a copy of a book
     */
    public void saveData()  {
        // TODO:
        // - Call method validateBook
        // - Generate code for the book
        // - Call method deleteBook of BookInfo
    }
}
/**
 * Created by hoang on 17/10/2017.
 */
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class BookBorrowHistory {
    private int borrowingID;
    private BorrowerInfo borrower;
    private BookInfo book;
    private Date expireTime;
    private String status;
    private Date dueDate;

    public BorrowerInfo getBorrower() {
        return this.borrower;
    }

    public BookInfo getBook() {
        return this.book;
    }

    /**
     * Update borrowing information to the database
     *
     * @param status is a string that indicate the new status of the book in database
     */
    public void updateBorrowingInfo(String status) {
        this.status = status;
    }

    /**
     * Get information of all book that match keyword on a specific catalogue or all catalogues
     *
     * @param keyword  a string that input to look up data
     * @param category a string that specifies catalogue on where to look up, can be "all" for all catalogues
     * @return return the ArrayList Bookinfo of all books that match
     * @throws SQLException if there was a problem when connect or getting data from database
     */
    public ArrayList<BookBorrowHistory> getBorrowingInfo(String keyword, String category) throws SQLException {

        return null;
    }

    /**
     * Get the expire time of all in process borrowing information associated with a borrower
     *
     * @return return the Date
     */
    public Date getExpireTime() {

        return this.expireTime;
    }

    /**
     * set the Expire Time to the next 2 days
     */
    public void setExpireTime() {
        this.expireTime = new Date();
    }

    /**
     * Update information to database
     *
     * @throws SQLException if there was a problem when connect or writing data to database
     */
    public void updateQuery () throws SQLException {

    }

    public void createQuey  () throws SQLException {

    }
}

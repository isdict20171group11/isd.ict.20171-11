import javax.management.Query;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by hoang on 17/10/2017.
 */
public class BookInfo  {
    private String title;
    private String author;
    private String publisher;
    private String type;
    private double price;
    private String bookID;
    private String status;

    private final String endpoint = "http://localhost/bookinfo";
    private final String keyLocation = ".keydb";

    /**
     * Init attribute
     * @param title title of the book
     * @param author author of the book
     * @param publisher publisher of the book
     * @param type type of the book
     * @param price price of the book
     * @param bookID id of the book
     */
    /**
     * Get information of all book that match keyword on a specific catalogue or all catalogues
     *
     * @param keyword  a string that input to look up data
     * @param category a string that specifies catalogue on where to look up, can be "all" for all catalogues
     * @return return the ArrayList Bookinfo of all books that match
     * @throws SQLException if there was a problem when connect or getting data from database
     */
    public ArrayList<BookInfo> getBookInfo(String keyword, String category) throws SQLException {

        return null;
    }

    /**
     * Update book information to the database
     *
     * @param status is a string that indicate the new status of the book in database
     */
    public void updateBookInfor(String status) {
        this.status = status;
    }

    /**
     * Update information to database
     *
     * @throws SQLException if there was a problem when connect or writing data to database
     */
    public void updateQuery() throws SQLException {

    }

    public BookInfo(String title, String author, String publisher, String type, double price, String bookID) {
        // TODO:
        // - init attribute
        // - connect to db
    }

    /**
     * Get the status of a book by book id
     *
     * @param bookID the id of book
     * @return BookInfo instance
     */
    public static BookInfo getBookByID(String bookID) {
        // TODO:
        // - query to DB
        // - return "Available" if found out, or vice versa
        return new BookInfo("", "", "", "", 0, "");
    }

    /**
     * Insert book information to db
     * @param book: book instance which need to be added
     * @throws InsertDBException if there are any errors
     */
}

/**
 * Created by hoang on 17/10/2017.
 */

public class BookCopyInfo  {
    private String copyID;
    private String author;
    private String type;
    private double price;
    private String bookID;

    private final String endpoint = "http://localhost/bookcopyinfo";
    private final String keyLocation = ".keydb";

    /**
     * Constructor
     * @param copyID id of copy
     * @param author author of copy
     * @param type type of copy
     * @param price price of copy
     * @param bookID orginal book id of copy
     */
    public BookCopyInfo(String copyID, String author, String type, double price, String bookID) {
        // TODO:
        // - init attribute
        // - connect to db
    }

    /**
     * Insert copy information to db
     * @param delete copy instance which need to be added
     */
    public static void deleteCopy(BookCopyInfo copy)  {
        // TODO:
        // - query to DB
        // - throw exception if it exist
    }
}
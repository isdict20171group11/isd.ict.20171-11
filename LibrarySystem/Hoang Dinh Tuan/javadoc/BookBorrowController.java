/**
 * Created by hoang on 17/10/2017.
 */
import java.sql.SQLException;
import java.util.ArrayList;

public class BookBorrowController {
    private ArrayList<BookInfo> book_list;

    /**
     * Get information of all book that match keyword on a specific catalogue or all catalogues
     *
     * @param keyword  a string that input to look up data
     * @param category a string that specifies catalogue on where to look up, can be "all" for all catalogues
     * @return ArrayList of BookInfo
     */
    public ArrayList<BookInfo> getBookInfo(String keyword, String category) {
        return this.book_list;
    }

    /**
     * Get number of current borrowing book of that User
     *
     * @param borrower a BorrowerInfo indicates Borrower using system
     * @return int
     */
    public int getNumberOfCurrentBook(BorrowerInfo borrower) {
        return borrower.getNumberCurrentBook();
    }

    /**
     * Update book status
     *
     * @param status a String indicates status of the book like "Borrowed"
     */
    public void updateBookInfo(String status) {
        for (BookInfo book : book_list) {
            book.updateBookInfor(status);
            try{
                book.updateQuery();
            } catch (SQLException e){
                System.out.printf("Error");
            }
        }
    }

    /**
     * Create new BorrowingInfo
     *
     * @param status a String indicates status of that Borrowing like "inProcess"
     */
    public void createBookBorrowHistỏy(String status) {
        BookBorrowHistory borrowingInfo = new BookBorrowHistory();
//        set borrowinginfo attribute
        try {
            borrowingInfo.updateQuery();
        } catch (SQLException e){
            System.out.printf("Error");
        }
    }

    /**
     * Update Borrower Information
     *
     * @param borrower a BorrowerInfo indicates borrower using system
     * @param select_list an ArrayList of BookInfo contains list of selected books
     * @param status boolean true if Borrower still can borrow, or false if Borrower can't

     */
    public void updateBorrowerInfo(BorrowerInfo borrower, ArrayList<BookInfo> select_list, boolean status) {
        borrower.setNumberCurrentBook(borrower.getNumberCurrentBook() + select_list.size());
        if (borrower.getNumberCurrentBook() == 5){
            borrower.setBorrowerStatus(status);
        }
        try{
            borrower.updateQuery();
        } catch (SQLException e){
            System.out.printf("Error");
        }
    }

    /**
     * Check if Borrower is able to borrow
     *
     * @param borrower a BorrowerInfo indicates borrower using system
     * @return true or false
     */
    public boolean checkBorrowerStatus(BorrowerInfo borrower) {
        return borrower.getBorrowerStatus();
    }

    /**
     * Warn Borrower time to collect book expired
     *
     * @param borrower a BorrowerInfo indicates borrower using system
     */
    public void warnExpireTime(BorrowerInfo borrower) {
//        get borrowing associated with borrower ExpireTime
//        if ExpireTime > Today
//        warn ExpireTime then call all update functions
    }
}


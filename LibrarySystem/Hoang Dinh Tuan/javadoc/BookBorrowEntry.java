/**
 * Created by hoang on 17/10/2017.
 */
import java.io.IOException;
import java.util.ArrayList;

public class BookBorrowEntry {
    private BorrowerInfo borrower;
    private ArrayList<BookInfo> select_list;
    private ArrayList<BookInfo> book_list;

    /**
     * Get information of all book that match keyword on a specific catalogue or all catalogues
     *
     * @param keyword  a string that input to look up data
     * @param category a string that specifies catalogue on where to look up, can be "all" for all catalogues
     * @return ArrayList of BookInfo
     */
    public ArrayList<BookInfo> getBookInfo(String keyword, String category) {
        BookBorrowController controller = new BookBorrowController();
        if (!this.book_list.isEmpty()) {
            this.book_list.clear();
        }
        this.book_list = controller.getBookInfo(keyword, category);
        return this.book_list;
    }

    /**
     * Add book to the select list
     *
     * @param book a BookInfo that is added to the select_list
     */
    public void selectBook(BookInfo book) {
        select_list.add(book);
    }

    /**
     * Remove book from the select list
     *
     * @param book a BookInfo that is removed from the select_list
     */
    public void unSelectBook(BookInfo book) {
        if (!select_list.isEmpty()) {
            for (int i = 0; i < select_list.size(); i++) {
                if (select_list.get(i).equals(book)) {
                    select_list.remove(select_list.get(i));
                }
            }
        }
    }

    /**
     * Display book info that match the search
     *
     * @exception IOException if there was a problem writing output
     */
    public void displayBookInfo() throws IOException{
        for (BookInfo book : book_list) {
            System.out.printf(".");
        }
    }

    /**
     * Checkout call sub Form for confirmation
     *
     */
    public void requestCheckout() {
        BookBorrowController controller = new BookBorrowController();
        if (!controller.checkBorrowerStatus(this.borrower)){
//            End process
            return;
        }
        if (select_list.size() + controller.getNumberOfCurrentBook(this.borrower) > 5){
//            can't requestCheckout
            return;
        }

        /**
         * Init confirm
         */
    }
}


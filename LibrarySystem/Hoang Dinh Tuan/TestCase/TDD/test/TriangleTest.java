import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 11/22/2017.
 */
public class TriangleTest {
    @Test
    public void isTriangle() throws Exception {
        Assert.assertEquals("Triangle", Triangle.isTriangle(3, 4, 5));
    }

    @Test
    public void isnotTriangle() throws Exception {
        Assert.assertEquals("Not Triangle", Triangle.isTriangle(6, 4, 12));
        Assert.assertEquals("Not Triangle", Triangle.isTriangle(-3, 8, 4));
    }

    @Test
    public void coverTest1() throws Exception {
        Assert.assertEquals("Not Triangle", Triangle.isTriangle(-3, 4, 5));
    }
    @Test
    public void coverTest2() throws Exception {
        Assert.assertEquals("Triangle", Triangle.isTriangle(3, 4, 5));
    }
    @Test
    public void coverTest3() throws Exception {
        Assert.assertEquals("Not Triangle", Triangle.isTriangle(3, 4, 12));
    }
}
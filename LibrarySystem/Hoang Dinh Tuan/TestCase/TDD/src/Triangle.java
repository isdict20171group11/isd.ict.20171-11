/**
 * Created by Administrator on 11/22/2017.
 */
public class Triangle {
    public static String isTriangle(double a, double b, double c) {
        if (a <= 0 || b <= 0 || c <= 0) {
            return "Not Triangle";
        } else if (a + b > c && b + c > a && c + a > b) {
            return "Triangle";
        }else return "Not Triangle";

    }
}



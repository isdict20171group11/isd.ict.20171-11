-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 25, 2017 lúc 05:15 SA
-- Phiên bản máy phục vụ: 10.1.21-MariaDB
-- Phiên bản PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `issuecard`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bookborrowhistory`
--

CREATE TABLE `bookborrowhistory` (
  `bookID` int(11) NOT NULL,
  `copyID` int(11) NOT NULL,
  `card_number` varchar(30) NOT NULL,
  `lentDate` date NOT NULL,
  `returnDate` date NOT NULL,
  `user_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bookcopyinfo`
--

CREATE TABLE `bookcopyinfo` (
  `copyID` int(10) NOT NULL,
  `bookID` int(10) NOT NULL,
  `author` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL,
  `price` float NOT NULL,
  `title` varchar(50) NOT NULL,
  `copy_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bookinfo`
--

CREATE TABLE `bookinfo` (
  `bookID` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `publisher` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL,
  `price` float NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `booklenthistory`
--

CREATE TABLE `booklenthistory` (
  `card_number` varchar(30) NOT NULL,
  `copyID` int(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `user_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `borrowerinfo`
--

CREATE TABLE `borrowerinfo` (
  `card_number` varchar(30) NOT NULL,
  `compensation` double NOT NULL,
  `user_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `card`
--

CREATE TABLE `card` (
  `card_number` varchar(30) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  `is_student` int(1) NOT NULL,
  `expired_date` varchar(30) NOT NULL,
  `activate_code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `user_id` varchar(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(50) NOT NULL,
  `date_of_birth` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `job` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bookborrowhistory`
--
ALTER TABLE `bookborrowhistory`
  ADD KEY `copy_history` (`copyID`),
  ADD KEY `book_history` (`bookID`),
  ADD KEY `cardno_history` (`card_number`),
  ADD KEY `userid_history` (`user_id`);

--
-- Chỉ mục cho bảng `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  ADD PRIMARY KEY (`copyID`),
  ADD KEY `book_constraint` (`bookID`);

--
-- Chỉ mục cho bảng `bookinfo`
--
ALTER TABLE `bookinfo`
  ADD PRIMARY KEY (`bookID`);

--
-- Chỉ mục cho bảng `booklenthistory`
--
ALTER TABLE `booklenthistory`
  ADD KEY `copy_number` (`copyID`),
  ADD KEY `card_number` (`card_number`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `borrowerinfo`
--
ALTER TABLE `borrowerinfo`
  ADD KEY `card_number` (`card_number`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_number`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  MODIFY `copyID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `bookinfo`
--
ALTER TABLE `bookinfo`
  MODIFY `bookID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `bookborrowhistory`
--
ALTER TABLE `bookborrowhistory`
  ADD CONSTRAINT `book_history` FOREIGN KEY (`bookID`) REFERENCES `bookcopyinfo` (`bookID`),
  ADD CONSTRAINT `cardno_history` FOREIGN KEY (`card_number`) REFERENCES `borrowerinfo` (`card_number`),
  ADD CONSTRAINT `copy_history` FOREIGN KEY (`copyID`) REFERENCES `bookcopyinfo` (`copyID`),
  ADD CONSTRAINT `userid_history` FOREIGN KEY (`user_id`) REFERENCES `borrowerinfo` (`user_id`);

--
-- Các ràng buộc cho bảng `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  ADD CONSTRAINT `book_constraint` FOREIGN KEY (`bookID`) REFERENCES `bookinfo` (`bookID`);

--
-- Các ràng buộc cho bảng `booklenthistory`
--
ALTER TABLE `booklenthistory`
  ADD CONSTRAINT `booklenthistory_ibfk_1` FOREIGN KEY (`copyID`) REFERENCES `bookcopyinfo` (`copyID`),
  ADD CONSTRAINT `booklenthistory_ibfk_2` FOREIGN KEY (`card_number`) REFERENCES `card` (`card_number`),
  ADD CONSTRAINT `booklenthistory_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Các ràng buộc cho bảng `borrowerinfo`
--
ALTER TABLE `borrowerinfo`
  ADD CONSTRAINT `borrowerinfo_ibfk_1` FOREIGN KEY (`card_number`) REFERENCES `card` (`card_number`),
  ADD CONSTRAINT `borrowerinfo_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Các ràng buộc cho bảng `card`
--
ALTER TABLE `card`
  ADD CONSTRAINT `card_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

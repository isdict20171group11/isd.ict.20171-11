import java.util.Scanner;

/**
 * Created by BuiAnhVu on 11/19/2017.
 */
public class Main {
    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a: ");
        int a = sc.nextInt();
        if(a == 0){
            System.out.println("a must not be zero");
            return;
        }
        System.out.print("Enter b: ");
        int b = sc.nextInt();
        System.out.print("Enter c: ");
        int c = sc.nextInt();
        System.out.println("The equation form: "+ a + "x^2 + " + b+"x + " + c + " = 0");
        EquationHandle equationHandle = new EquationHandle(a, b, c);
        double[] result = equationHandle.solve();
        if(result[0] == 1 ){
            System.out.println("There are two solution");
            double x1 = result[1];
            double x2 = result[2];
            System.out.println("X1 = " + x1);
            System.out.println("X2 = " + x2);
        }
        else if( result[0] == -1){
            System.out.println("There are no solution");
        }
        else if(result[0] == 0){
            System.out.println("There is only one solution");
            double x = result[1];
            System.out.println("X = " + x);
        }

    }
}

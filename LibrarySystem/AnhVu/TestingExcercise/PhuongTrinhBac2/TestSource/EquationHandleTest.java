import org.junit.Test;

import static org.junit.Assert.*;

public class EquationHandleTest {
    @Test
    public void solve() throws Exception {

        // test x2 + 2x + 1 = 0
        EquationHandle equationHandle = new EquationHandle(1,2,1);
        double[] result = {0,-1,-1}; // 0: 1 nghiem, -1 -1 => x1 = x2 = -1
        assertArrayEquals(result, equationHandle.solve(), 0.2);

        equationHandle = new EquationHandle(3,2,5);
        double[] result_1 = {-1, 0, 0};// -1: vo nghiem, 0 0 default initialze value
        assertArrayEquals(result_1, equationHandle.solve(), 0.2);

        equationHandle = new EquationHandle(1,-2,-8);
        double[] result_2 = {1, 4, -2}; // 1: 2 nghiem x1 = 5, x2 = 1
        assertArrayEquals(result_2, equationHandle.solve(),0.2);

        //test a = 0
        equationHandle = new EquationHandle(0,-2,-8);
        assertArrayEquals(null, equationHandle.solve(), 0.2);

    }

}
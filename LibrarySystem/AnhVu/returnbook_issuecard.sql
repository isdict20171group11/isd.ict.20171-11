-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2017 at 04:55 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookcopyinfo`
--

CREATE TABLE `bookcopyinfo` (
  `copyID` int(10) NOT NULL,
  `bookID` int(10) NOT NULL,
  `author` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookinfo`
--

CREATE TABLE `bookinfo` (
  `bookID` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `publisher` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `booklenthistory`
--

CREATE TABLE `booklenthistory` (
  `card_number` varchar(30) NOT NULL,
  `copyID` int(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `user_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `borrowerinfo`
--

CREATE TABLE `borrowerinfo` (
  `card_number` varchar(30) NOT NULL,
  `compensation` double NOT NULL,
  `user_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_number` varchar(30) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `is_active` int(1) NOT NULL,
  `is_student` int(1) NOT NULL,
  `expired_date` varchar(30) NOT NULL,
  `activate_code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` varchar(20) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(50) NOT NULL,
  `date_of_birth` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `job` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  ADD PRIMARY KEY (`copyID`),
  ADD KEY `book_constraint` (`bookID`);

--
-- Indexes for table `bookinfo`
--
ALTER TABLE `bookinfo`
  ADD PRIMARY KEY (`bookID`);

--
-- Indexes for table `booklenthistory`
--
ALTER TABLE `booklenthistory`
  ADD KEY `copy_number` (`copyID`),
  ADD KEY `card_number` (`card_number`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `borrowerinfo`
--
ALTER TABLE `borrowerinfo`
  ADD KEY `card_number` (`card_number`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_number`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  MODIFY `copyID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bookinfo`
--
ALTER TABLE `bookinfo`
  MODIFY `bookID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookcopyinfo`
--
ALTER TABLE `bookcopyinfo`
  ADD CONSTRAINT `book_constraint` FOREIGN KEY (`bookID`) REFERENCES `bookinfo` (`bookID`);

--
-- Constraints for table `booklenthistory`
--
ALTER TABLE `booklenthistory`
  ADD CONSTRAINT `booklenthistory_ibfk_1` FOREIGN KEY (`copyID`) REFERENCES `bookcopyinfo` (`copyID`),
  ADD CONSTRAINT `booklenthistory_ibfk_2` FOREIGN KEY (`card_number`) REFERENCES `card` (`card_number`),
  ADD CONSTRAINT `booklenthistory_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `borrowerinfo`
--
ALTER TABLE `borrowerinfo`
  ADD CONSTRAINT `borrowerinfo_ibfk_1` FOREIGN KEY (`card_number`) REFERENCES `card` (`card_number`),
  ADD CONSTRAINT `borrowerinfo_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `card`
--
ALTER TABLE `card`
  ADD CONSTRAINT `card_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package com.itss.exception;

/**
 * Created by HarDToBelieve on 10/17/2017.
 */
public class AddBookException extends Exception {
    public AddBookException(String msg) {
        super(msg);
    }
}

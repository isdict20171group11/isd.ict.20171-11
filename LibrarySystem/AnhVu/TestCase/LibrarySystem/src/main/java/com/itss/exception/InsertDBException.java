package com.itss.exception;

/**
 * Created by HarDToBelieve on 10/17/2017.
 */
public class InsertDBException extends Exception {
    public InsertDBException(String msg) {
        super(msg);
    }
}

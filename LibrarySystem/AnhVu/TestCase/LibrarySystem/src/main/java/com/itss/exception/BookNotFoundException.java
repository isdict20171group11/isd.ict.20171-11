package com.itss.exception;

/**
 * Created by HarDToBelieve on 10/17/2017.
 */
public class BookNotFoundException extends Exception {
    public BookNotFoundException(String msg) {
        super(msg);
    }
}

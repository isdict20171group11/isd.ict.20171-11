package com.itss.exception;

/**
 * Created by HarDToBelieve on 10/17/2017.
 */
public class AddCopyException extends Exception {
    public AddCopyException(String msg) {
        super(msg);
    }
}

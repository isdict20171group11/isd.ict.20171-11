public class EquationHandle {
    int a;
    int b;
    int c;
    double delta;
    public EquationHandle(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
        this.delta = b*b - 4*a*c;
    }
    public double[] solve(){
        double[] result = new double[3]; // double[1] = x1; double[2] = x2 result[0] = delta
       if(a == 0){
           return null;
       }else if(delta == 0){
           double x = -(double) b / (2 *a);
           result[0] = 0;
           result[1] = x;
           result[2] = x;
       }else if(delta < 0){
           result[0] = -1;
       }else {
           double x1 = (-1*b + Math.sqrt(delta))/(2*a);
           double x2 = (-1*b - Math.sqrt(delta))/(2*a);
           result[0] = 1;
           result[1] = x1;
           result[2] = x2;
       }
       return result;
    }

}
